use hdi::prelude::*;
pub mod file;
pub use file::validate_create_file;
pub use file::File;
pub mod file_chunk;
pub use file_chunk::FileChunk;

#[hdk_entry_defs]
#[unit_enum(UnitEntryTypes)]
pub enum EntryTypes {
    File(File),
    FileChunk(FileChunk),
}

#[hdk_link_types]
pub enum LinkTypes {
    CommitHashToFile,
    AllFiles,
    AllFileChunks,
}

#[hdk_extern]
pub fn genesis_self_check(_data: GenesisSelfCheckData) -> ExternResult<ValidateCallbackResult> {
    Ok(ValidateCallbackResult::Valid)
}

#[hdk_extern]
pub fn validate(op: Op) -> ExternResult<ValidateCallbackResult> {
    match op.flattened::<EntryTypes, LinkTypes>()? {
        FlatOp::StoreEntry(store_entry) => match store_entry {
            OpEntry::CreateEntry { app_entry, action } => match app_entry {
                EntryTypes::File(file) => file::validate_create_file(file, action),
                EntryTypes::FileChunk(file_chunk) => {
                    file_chunk::validate_create_file_chunk(file_chunk, action)
                }
            },
            _ => Ok(ValidateCallbackResult::Valid),
        },
        _ => Ok(ValidateCallbackResult::Valid),
    }
}
