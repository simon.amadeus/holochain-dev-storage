use hdi::prelude::*;

use crate::FileChunk;

#[hdk_entry_helper]
#[derive(Clone)]
pub struct File {
    pub kind: FileKind,
    pub size: u64,
    pub file_hash: String,
    pub preserved_path: Vec<String>,
    pub compression: FileCompression,
    pub file_chunks: Vec<EntryHash>,
}

#[derive(Clone, Debug, Serialize, Deserialize, SerializedBytes)]
pub enum FileKind {
    ContentFile,
    DatabaseStructureDump,
    DatabaseContentDump,
}

#[derive(Clone, Debug, Serialize, Deserialize, SerializedBytes, PartialEq)]
pub enum FileCompression {
    None,
    Gzip,
}

// validations
// TODO: only one dump file set per commit hash
// TODO: handle orphaned file chunks

pub fn validate_create_file(file: File, _action: Create) -> ExternResult<ValidateCallbackResult> {
    if file.size == 0 {
        return Ok(ValidateCallbackResult::Invalid(
            "File size cannot be zero".to_string(),
        ));
    }
    if file.file_hash.is_empty() {
        return Ok(ValidateCallbackResult::Invalid(
            "File hash cannot be empty".to_string(),
        ));
    }
    if file.preserved_path.is_empty() {
        return Ok(ValidateCallbackResult::Invalid(
            "Preserved path cannot be empty".to_string(),
        ));
    }
    if file.file_chunks.is_empty() {
        return Ok(ValidateCallbackResult::Invalid(
            "File chunks cannot be empty".to_string(),
        ));
    }

    match file.kind {
        FileKind::DatabaseStructureDump | FileKind::DatabaseContentDump => {
            if file.compression == FileCompression::None {
                return Ok(ValidateCallbackResult::Invalid(
                    "Database dumps must be compressed".to_string(),
                ));
            }
        }
        _ => {}
    }

    match file.compression {
        FileCompression::Gzip => {
            let first_chunk_entry_hash: EntryHash = file.file_chunks[0].clone();
            let entry = must_get_entry(first_chunk_entry_hash)?.content;
            let first_chunk: FileChunk = entry.try_into()?;
            if first_chunk.bytes.len() < 2 {
                return Ok(ValidateCallbackResult::Invalid(
                    "Gzip file chunks must be at least 2 bytes".to_string(),
                ));
            }
            if &first_chunk.bytes[..2] != &[0x1f, 0x8b] {
                return Ok(ValidateCallbackResult::Invalid(
                    "Gzipped file must start with 0x1f8b".to_string(),
                ));
            }
        }
        _ => {}
    }

    Ok(ValidateCallbackResult::Valid)
}
