use hdi::prelude::*;

#[hdk_entry_helper]
#[derive(Clone)]
pub struct FileChunk {
    pub bytes: Vec<u8>,
}

pub fn validate_create_file_chunk(
    file_chunk: FileChunk,
    _action: Create,
) -> ExternResult<ValidateCallbackResult> {
    if file_chunk.bytes.is_empty() {
        return Ok(ValidateCallbackResult::Invalid(
            "File chunk bytes cannot be empty".to_string(),
        ));
    }

    Ok(ValidateCallbackResult::Valid)
}
