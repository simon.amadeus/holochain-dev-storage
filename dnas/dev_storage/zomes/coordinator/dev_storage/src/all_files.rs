use dev_storage_integrity::File;
use dev_storage_integrity::LinkTypes;
use hdk::prelude::*;

#[hdk_extern]
pub fn get_files_by_commit_hash(
    input: GetFilesByCommitHashInput,
) -> ExternResult<GetFilesByCommitHashOutput> {
    let path = Path::from(&input.commit_hash);
    let links = get_links(path.path_entry_hash()?, LinkTypes::CommitHashToFile, None)?;

    let targets: Vec<EntryHash> = links
        .iter()
        .map(|link| link.target.clone().into())
        .collect();

    let mut records: Vec<Record> = Vec::new();

    for target in targets {
        let record: Record = get(target, GetOptions::default())?.ok_or(wasm_error!(
            WasmErrorInner::Guest("Could not find the newly created file".to_string(),)
        ))?;
        records.push(record);
    }

    let files: Vec<File> = records
        .into_iter()
        .map(|record| {
            match record.entry().to_app_option::<File>().map_err(|_| {
                wasm_error!(WasmErrorInner::Guest(
                    "Could not parse the newly created file entry".to_string(),
                ))
            })? {
                Some(file) => Ok(file),
                None => Err(wasm_error!(WasmErrorInner::Guest(
                    "Could not find the newly created file".to_string(),
                ))),
            }
        })
        .collect::<Result<Vec<File>, _>>()?;

    let output: GetFilesByCommitHashOutput = GetFilesByCommitHashOutput { files: files };

    Ok(output)
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone)]
pub struct GetFilesByCommitHashInput {
    pub commit_hash: String,
}

#[derive(Serialize, Deserialize, SerializedBytes, Debug, Clone)]
pub struct GetFilesByCommitHashOutput {
    pub files: Vec<File>,
}

#[hdk_extern]
pub fn get_all_file_hashes(_: ()) -> ExternResult<GetAllFileHashesOutput> {
    let path: Path = Path::from("all_files");
    let links: Vec<Link> = get_links(path.path_entry_hash()?, LinkTypes::AllFiles, None)?;

    let mut file_hashes: Vec<String> = Vec::new();

    for link in links {
        let file_entry_hash: EntryHash = link.target.clone().into();
        let file_record: Record = match get(file_entry_hash, GetOptions::default())? {
            Some(record) => record,
            None => {
                return Err(wasm_error!(WasmErrorInner::Guest(
                    "Could not find the file".to_string(),
                )))
            }
        };

        let file: File = match file_record.entry().to_app_option().map_err(|_| {
            wasm_error!(WasmErrorInner::Guest(
                "Could not convert entry to app option".to_string(),
            ))
        })? {
            Some(file) => file,
            None => {
                return Err(wasm_error!(WasmErrorInner::Guest(
                    "Could not find the file".to_string(),
                )))
            }
        };

        file_hashes.push(file.file_hash);
    }

    let output: GetAllFileHashesOutput = GetAllFileHashesOutput { file_hashes };

    Ok(output)
}

#[derive(Clone, Serialize, Deserialize, SerializedBytes, Debug)]
pub struct GetAllFileHashesOutput {
    pub file_hashes: Vec<String>,
}
