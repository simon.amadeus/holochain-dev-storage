use dev_storage_integrity::{file_chunk::FileChunk, EntryTypes};
use hdk::prelude::*;

pub fn create_file_chunk(file_chunk: FileChunk) -> ExternResult<Record> {
    let action_hash: ActionHash = create_entry(&EntryTypes::FileChunk(file_chunk.clone()))?;
    let record: Record = get(action_hash.clone(), GetOptions::default())?.ok_or(wasm_error!(
        WasmErrorInner::Guest("Could not find the newly created file".to_string(),)
    ))?;

    Ok(record)
}

#[hdk_extern]
pub fn store_file_chunk(input: StoreFileChunkInput) -> ExternResult<StoreFileChunkOutput> {
    let file_chunk = FileChunk {
        bytes: input.file_chunk.bytes,
    };

    let file_chunk_entry_hash: EntryHash = hash_entry(&file_chunk)?;

    let _file_chunk_record: Record =
        match get(file_chunk_entry_hash.clone(), GetOptions::default())? {
            Some(record) => record,
            None => {
                create_entry(&EntryTypes::FileChunk(file_chunk))?;
                get(file_chunk_entry_hash.clone(), GetOptions::default())?.ok_or(wasm_error!(
                    WasmErrorInner::Guest(
                        "Could not find the newly created file chunk".to_string(),
                    )
                ))?
            }
        };

    Ok(StoreFileChunkOutput {
        file_chunk_entry_hash,
    })
}

#[derive(Clone, Serialize, Deserialize, SerializedBytes, Debug)]
pub struct StoreFileChunkInput {
    pub file_chunk: FileChunk,
}

#[derive(Clone, Serialize, Deserialize, SerializedBytes, Debug)]
pub struct StoreFileChunkOutput {
    pub file_chunk_entry_hash: EntryHash,
}

#[hdk_extern]
pub fn get_file_chunk(input: GetFileChunkInput) -> ExternResult<GetFileChunkOutput> {
    let file_chunk_record: Record =
        get(input.entry_hash, GetOptions::default())?.ok_or(wasm_error!(WasmErrorInner::Guest(
            "Could not find the file chunk".to_string(),
        )))?;

    let file_chunk: FileChunk = file_chunk_record
        .entry()
        .to_app_option::<FileChunk>()
        .map_err(|_| {
            wasm_error!(WasmErrorInner::Guest(
                "Could not parse the file chunk entry".to_string(),
            ))
        })?
        .ok_or(wasm_error!(WasmErrorInner::Guest(
            "Could not find the file chunk".to_string(),
        )))?;

    Ok(GetFileChunkOutput { file_chunk })
}

#[derive(Clone, Serialize, Deserialize, SerializedBytes, Debug)]
pub struct GetFileChunkInput {
    pub entry_hash: EntryHash,
}

#[derive(Clone, Serialize, Deserialize, SerializedBytes, Debug)]
pub struct GetFileChunkOutput {
    pub file_chunk: FileChunk,
}
