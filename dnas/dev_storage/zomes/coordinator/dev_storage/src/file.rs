use dev_storage_integrity::{file::File, EntryTypes, LinkTypes};
use hdk::prelude::*;

pub fn create_file(file: File) -> ExternResult<Record> {
    let entry_hash: EntryHash = hash_entry(&file)?;
    let action_hash: ActionHash = create_entry(&EntryTypes::File(file))?;
    let record: Record = get(action_hash.clone(), GetOptions::default())?.ok_or(wasm_error!(
        WasmErrorInner::Guest("Could not find the newly created file".to_string(),)
    ))?;

    let path: Path = Path::from("all_files");
    create_link(path.path_entry_hash()?, entry_hash, LinkTypes::AllFiles, ())?;

    Ok(record)
}

#[hdk_extern]
pub fn store_file(input: StoreFileInput) -> ExternResult<StoreFileOutput> {
    let file = File::from(&input);

    let file_entry_hash: EntryHash = hash_entry(&file)?;

    let _file_record: Record = match get(file_entry_hash.clone(), GetOptions::default())? {
        Some(record) => record,
        None => {
            create_entry(&EntryTypes::File(file))?;
            get(file_entry_hash.clone(), GetOptions::default())?.ok_or(wasm_error!(
                WasmErrorInner::Guest("Could not find the newly created file chunk".to_string(),)
            ))?
        }
    };

    let path: Path = Path::from("all_files");
    create_link(
        path.path_entry_hash()?,
        file_entry_hash.clone(),
        LinkTypes::AllFiles,
        (),
    )?;

    let path: Path = Path::from(input.commit_hash);
    create_link(
        path.path_entry_hash()?,
        file_entry_hash.clone(),
        LinkTypes::CommitHashToFile,
        (),
    )?;

    Ok(StoreFileOutput { file_entry_hash })
}

#[derive(Clone, Serialize, Deserialize, SerializedBytes, Debug)]
pub struct StoreFileInput {
    pub commit_hash: String,
    pub file: File,
}

#[derive(Clone, Serialize, Deserialize, SerializedBytes, Debug)]
pub struct StoreFileOutput {
    pub file_entry_hash: EntryHash,
}

impl From<&StoreFileInput> for File {
    fn from(input: &StoreFileInput) -> Self {
        input.file.clone()
    }
}
