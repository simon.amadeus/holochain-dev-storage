use hdk::prelude::*;
pub mod all_files;
pub mod file;
pub mod file_chunk;

#[hdk_extern]
pub fn init(_: ()) -> ExternResult<InitCallbackResult> {
    Ok(InitCallbackResult::Pass)
}
